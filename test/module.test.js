const wxWorkLogin = require('../src/index');
const sinon = require('sinon');
const should = require('should');
const APIs = require('../src/apis');
const { reject } = require('lodash');

function sleepMs(ms) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve();
        }, ms);
    });
}

describe('module测试', function () {
    let getUserIdForLoginStub = null;
    let getMemberStub = null;
    beforeEach(function () {
        getUserIdForLoginStub = sinon.stub(APIs.prototype, 'getUserIdForLogin');
        getMemberStub = sinon.stub(APIs.prototype, 'getMember');
    });

    afterEach(function () {
        getUserIdForLoginStub.restore();
        getMemberStub.restore();
    });

    it('无效的参数初始化中间件时将出错', function () {
        (() => {
            wxWorkLogin({});
        }).should.throw();
    });

    it('使用错误的token获取方法初始化中间件时将出错', function () {
        (() => {
            wxWorkLogin({});
        }).should.throw(/错误的token获取方法/);
    });

    it('state状态检查失败时，将返回错误', async function () {
        let asyncGetTokenFn = sinon.fake.resolves('token');
        let checkStateFn = sinon.fake.resolves(false);
        let wxWorkLoginPlugin = wxWorkLogin({ asyncGetTokenFn, checkStateFn });
        let req = { query: { code: 'code', state: 'errState' } };
        let res = {};
        res.status = sinon.fake.returns(res);
        res.send = sinon.spy();
        let next = sinon.spy();
        wxWorkLoginPlugin(req, res, next);
        await sleepMs(10);
        sinon.assert.calledOnceWithMatch(res.status, 403);
        sinon.assert.calledOnceWithMatch(
            res.send,
            sinon.match(/错误的授权参数/)
        );
        getUserIdForLoginStub.callCount.should.equal(0);
        getMemberStub.callCount.should.equal(0);
        next.callCount.should.equal(0);
    });

    it('参数中不存在code时，将返回错误', async function () {
        let asyncGetTokenFn = sinon.fake.resolves('token');
        let checkStateFn = sinon.fake.resolves(false);
        let wxWorkLoginPlugin = wxWorkLogin({ asyncGetTokenFn, checkStateFn });
        let req = { query: { state: 'state' } };
        let res = {};
        res.status = sinon.fake.returns(res);
        res.send = sinon.spy();
        let next = sinon.spy();
        wxWorkLoginPlugin(req, res, next);
        await sleepMs(10);
        sinon.assert.calledOnceWithMatch(res.status, 403);
        sinon.assert.calledOnceWithMatch(res.send, sinon.match(/未经授权/));
        getUserIdForLoginStub.callCount.should.equal(0);
        getMemberStub.callCount.should.equal(0);
        next.callCount.should.equal(0);
    });

    it('获取token出错时将返回错误', async function () {
        let asyncGetTokenFn = sinon.fake.rejects(new Error('error token'));
        let wxWorkLoginPlugin = wxWorkLogin({ asyncGetTokenFn });
        let req = { query: { code: 'code', state: 'state' } };
        let res = {};
        res.status = sinon.fake.returns(res);
        res.send = sinon.spy();
        let next = sinon.spy();
        wxWorkLoginPlugin(req, res, next);
        await sleepMs(10);
        sinon.assert.calledOnceWithMatch(res.status, 400);
        sinon.assert.calledOnceWithMatch(res.send, sinon.match(/error token/));
        getUserIdForLoginStub.callCount.should.equal(0);
        getMemberStub.callCount.should.equal(0);
        next.callCount.should.equal(0);
    });

    it('错误的token将无法获取用户信息', async function () {
        let asyncGetTokenFn = sinon.fake.resolves('errtoken');
        let wxWorkLoginPlugin = wxWorkLogin({ asyncGetTokenFn });
        let req = { query: { code: 'code', state: 'state' } };
        let res = {};
        res.status = sinon.fake.returns(res);
        res.send = sinon.spy();
        let next = sinon.spy();
        getUserIdForLoginStub
            .withArgs('errtoken', 'code')
            .rejects(new Error('invalid token'));
        wxWorkLoginPlugin(req, res, next);
        await sleepMs(10);
        sinon.assert.calledOnceWithMatch(res.status, 400);
        sinon.assert.calledOnceWithMatch(
            res.send,
            sinon.match(/invalid token/)
        );
        getUserIdForLoginStub.callCount.should.equal(1);
        getMemberStub.callCount.should.equal(0);
        next.callCount.should.equal(0);
    });

    it('正确获取企业成员信息', async function () {
        let asyncGetTokenFn = sinon.fake.resolves('token');
        let checkStateFn = sinon.fake.resolves(true);
        let wxWorkLoginPlugin = wxWorkLogin({ asyncGetTokenFn, checkStateFn });
        let req = { query: { code: 'code', state: 'state' } };
        let res = {};
        res.status = sinon.fake.returns(res);
        res.send = sinon.spy();
        let next = sinon.spy();
        let userInfo = { userId: 'user1', name: 'uname' };
        getUserIdForLoginStub.withArgs('token', 'code').resolves('user1');
        getMemberStub.withArgs('token', 'user1').resolves(userInfo);
        wxWorkLoginPlugin(req, res, next);
        await sleepMs(20);
        next.callCount.should.equal(1);
        getUserIdForLoginStub.callCount.should.equal(1);
        getMemberStub.callCount.should.equal(1);
        should.exist(req.qymember);
        req.qymember.should.be.equal(userInfo);
        sinon.assert.notCalled(res.status);
        sinon.assert.notCalled(res.send);
    });
});
