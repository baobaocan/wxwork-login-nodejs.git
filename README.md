# wxwork-login-nodejs

#### 介绍
nodejs企业微信登录模块

#### 软件架构
作为express的中间件


#### 安装教程
npm install @chadegushi/wxwork-login-nodejs

#### 使用说明

在express框架下使用

```nodejs
const wxworkLogin = require('@chadegushi/wxwork-login-nodejs');
// 获取企业微信access token的异步方法
async function asyncGetTokenFn() {
	return new Promise.resolve('access token')
}

// 验证企业微信回调中的state参数合法性，合法返回true，否则返回false
// req为express中的request对象
async function checkStateFn(state, req) {
	return true
}

let loginMiddler = wxworkLogin({asyncGetTokenFn, checkStateFn});
// 在需要处理企业微信扫码登录回调地址处加入该中间件
route.use(/wxworkcallback, loginMiddler, (req, res) => {
	// 如果登录的用户是企业正式员工，则res.qymember就可以得到成员信息
	console.log(res.qymember)
})
...
```

#### 参与贡献

#### 特技