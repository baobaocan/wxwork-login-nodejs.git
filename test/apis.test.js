const should = require('should');
const sinon = require('sinon');
const axios = require('axios');
const WxWorkApi = require('../src/apis');

describe('test apis of wxwork', function () {
    beforeEach(function () {
        createStub = sinon.stub(axios, 'create');
    });

    afterEach(function () {
        createStub.restore();
    });

    it('创建api接口对象', function () {
        createStub.returns({});
        let api = new WxWorkApi();
        sinon.assert.callCount(createStub, 1);
        sinon.assert.calledWith(
            createStub,
            sinon.match({
                baseURL: 'https://qyapi.weixin.qq.com/cgi-bin',
                timeout: 5000,
                headers: sinon.match({ 'Content-Type': 'application/json' }),
            })
        );
    });

    it('获取登录用户的id', async function () {
        let instance = { get: async () => {} };
        let getStub = sinon.stub(instance, 'get');
        let token = 'token';
        let code = 'code123';
        let userId = 'user12';
        getStub
            .withArgs(
                '/user/getuserinfo',
                sinon.match({ params: { access_token: token, code: code } })
            )
            .resolves({ status: 200, data: { errcode: 0, UserId: userId } });
        createStub.returns(instance);
        let api = new WxWorkApi();
        let uid = await api.getUserIdForLogin(token, code);
        uid.should.be.equal(userId);
        createStub.callCount.should.be.equal(1);
        getStub.callCount.should.be.equal(1);
    });

    it('当登录用户并非企业成员时，获取登录用户的id将抛出错误', async function () {
        let instance = { get: async () => {} };
        let getStub = sinon.stub(instance, 'get');
        let token = 'token';
        let code = 'code123';
        let OpenId = 'openid12';
        getStub
            .withArgs(
                '/user/getuserinfo',
                sinon.match({ params: { access_token: token, code: code } })
            )
            .resolves({ status: 200, data: { errcode: 0, OpenId: OpenId} });
        createStub.returns(instance);
        let api = new WxWorkApi();
        await api.getUserIdForLogin(token, code).should.be.rejectedWith(/非当前企业用户/);
        createStub.callCount.should.be.equal(1);
        getStub.callCount.should.be.equal(1);
    });

    it('错误的code将抛出错误信息', async function () {
        let instance = { get: async () => {} };
        let getStub = sinon.stub(instance, 'get');
        let token = 'token';
        let code = 'code123';
        let userId = 'user12';
        getStub
            .withArgs(
                '/user/getuserinfo',
                sinon.match({ params: { access_token: token, code: code } })
            )
            .resolves({
                status: 200,
                data: {
                    errcode: 40029,
                    errmsg: 'invalid code',
                },
            });
        createStub.returns(instance);
        let api = new WxWorkApi();

        await api
            .getUserIdForLogin(token, code)
            .should.be.rejectedWith(/invalid code/);

        getStub.callCount.should.be.equal(1)
        createStub.callCount.should.be.equal(1)
    });

    it('获取企业成员的完整信息', async function () {
        let instance = { get: async () => {} };
        let getStub = sinon.stub(instance, 'get');
        let token = 'token';
        let user = { userid: 'user12', name: 'name', status: 1 };
        getStub
            .withArgs(
                '/user/get',
                sinon.match({
                    params: { access_token: token, userid: user.userid },
                })
            )
            .resolves({
                status: 200,
                data: Object.assign({ errcode: 0 }, user),
            });
        createStub.returns(instance);
        let api = new WxWorkApi();
        let member = await api.getMember(token, user.userid);
        member.should.be.eql(user);
        createStub.callCount.should.be.equal(1);
        getStub.callCount.should.be.equal(1);
    });
});
